---
attachments: [u_models_baryon.png, u_models_pion.png]
favorited: true
title: PROC FIT
created: '2021-01-30T19:43:09.514Z'
modified: '2021-01-30T20:21:19.334Z'
---



# PROC FIT

Параметры ужинского

Baryons:
| PROC# | A1    | B1   | A2     | B2  | A3  | ATOP | YMIN |
| ----- | ----- | ---- | ------ | --- | --- | ---- | ---- |
| PROC0 | 13.71 | 1.75 | -30.69 | 3.0 | 0.0 | 1.0  | 0.93 |
| PROC1 | 25.0  | 1.0  | -50.34 | 1.5 | 0.0 | 0.0  | 1.4  |
| PROC4 | 0.6   | 0.0  | -1.2   | 0.5 | 0.0 | 0.0  | 1.4  |

Pions:
| PROC# | A1    | B1  | A2     | B2  | A3  | ATOP | YMIN |
| ----- | ----- | --- | ------ | --- | --- | ---- | ---- |
| PROC0 | 150.0 | 1.8 | -247.3 | 2.3 | 0.0 | 1.0  | 2.3  |
| PROC1 | 5.77  | 0.6 | -5.77  | 0.8 | 0.0 | 0.0  | 0.0  |
| PROC4 | 1.0   | 0.0 | -11.02 | 1.0 | 0.0 | 0.0  | 2.4  |

PROC = (y \< YMIN) ? ATOP : A1 * exp(-B1 * y) + A2 * exp(-B2 * y) + A3

ExchangeWithExcitation = PROC1 \* PROC4
ExchangeWithoutExcitaiton = PROC0

We can introduce functions

exchange(y) = a * exp(-b * y) + c
excitation(y) = a * exp(-b * y) + c

So `ExchangeWithExcitation = Exchange(y) * excitation(y)` and `ExchangeWithoutExcitation = Exchange(y) * (1 - excitation(y))`

PION FIT PARAMETERS

| P          | A                           | B                            | C                             |
| ---------- | --------------------------- | ---------------------------- | ----------------------------- |
| exchange   | 5.10106          +/- 0.1429 | 0.707086         +/- 0.01015 | 0.0225197        +/- 0.002591 |
| excitation | -8.08486         +/- 0.3508 | 0.829443         +/- 0.01572 | 1.01576          +/- 0.003599 |

BARYON FIT PARAMETERS

| P          | A                            | B                             | C                             |
| ---------- | ---------------------------- | ----------------------------- | ----------------------------- |
| exchange   | 1.60445          +/- 0.01943 | 0.568653         +/- 0.007952 | -0.0130699       +/- 0.002562 |
| excitation | -4.19488         +/- 0.1441  | 0.928085         +/- 0.01909  | 1.00589          +/- 0.003479 |

# ВЫВОД

для pion и baryons можно убрать PROC4 и в PROC[0,1] вместо 5 параметров использовать 3. т.е. в общем убрать 
`2*(5 + 2*2) = 18` parameters
