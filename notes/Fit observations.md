---
title: Fit observations
created: '2021-02-01T19:16:47.311Z'
modified: '2021-02-01T20:11:28.121Z'
---

# Fit observations

PROC1 has no effect ...

## NA61pC BARYON
 * A1 increase to ~ 3
 * B1 descrease to `[0.3:0.5]
 * PROC1 has no effect

## NA61PimC PION
 * B1 descrease `[0.3:0.6]

## NA61pp (p @ 80GeV)
 * A1 increase maybe to 5
 * рост B1 понижает при ~0 и увеличивает по краям. я бы попробовал снизить до 0.3

